public class Task3 {
    private double sideA, sideB, sideC;

    public Task3(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public Task3() {
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }

        Task3 triangle = (Task3) o;

        if (Double.compare(triangle.getSideA(), getSideA()) != 0){
            return false;
        }
        if (Double.compare(triangle.getSideB(), getSideB()) != 0){
            return false;
        }
        return Double.compare(triangle.getSideC(), getSideC()) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getSideA());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getSideB());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getSideC());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "sideA=" + sideA +
                ", sideB=" + sideB +
                ", sideC=" + sideC +
                '}';
    }

    public double calculateArea(){
        double semiPerimeter = calculatePerimeter() / 2;
        double area = Math.sqrt(semiPerimeter * (semiPerimeter-sideA) *
                (semiPerimeter-sideB) * (semiPerimeter-sideC));
        return area;
    }

    public double calculatePerimeter(){
        double perimeter = sideA+sideB+sideC;
        return perimeter;

    }
}