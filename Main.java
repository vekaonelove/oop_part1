public class Main {
    public static void main(String[] args) {
        //Task1
        System.out.println("Task1:");

        Test1 test1 = new Test1(12,13);
        int sum = test1.calculateSum();
        int biggest = test1.calculateBiggest();

        System.out.println(test1);
        System.out.println(biggest);
        System.out.println(sum);

        System.out.println("\n");

        //Task2
        System.out.println("Task2:");

        Test2 test2 = new Test2(15,16);

        System.out.println(test2);
        System.out.println(test2.getA());
        System.out.println(test2.getB());

        System.out.println("\n");

        //Task3
        System.out.println("Task3:");

        Task3 first = new Task3(1,3,3);

        double firstPerimeter = first.calculatePerimeter();
        double firstArea = first.calculateArea();

        System.out.println(first);
        System.out.println("Perimeter: " + firstPerimeter);
        System.out.println("Area: " + firstArea);

        System.out.println("\n");

        //Task 4
        System.out.println("Task4:");
        Task4 counter = new Task4(5, 10, 6);

        System.out.println("Counter: " + counter.value());

        counter.increaseByOne();

        System.out.println("Counter increased: " + counter.value());

        counter.decreaseByOne();

        System.out.println("Counter decreased: " + counter.value());

        System.out.println("\n");

        //Task 5
        System.out.println("Task5:");
        Task5 time1 = new Task5(16,3,45);
        System.out.println(time1);


        time1.addHours(4);
        time1.addMinutes(18);
        time1.addSeconds(1);


        System.out.println(time1);


        System.out.println("\n");

    }

}

