public class Test1 {
    private int a;
    private int b;

    public Test1(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int calculateSum(){
        int sum = a + b;
        return sum;
    }

    public int calculateBiggest(){
        if( a > b ){return a;}
        return b;
    }

    public void printA(){
        System.out.println(a);
    }

    public void printB(){
        System.out.println(b);
    }

    @Override
    public String toString() {
        return "Test1{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
