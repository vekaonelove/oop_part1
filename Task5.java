public class Task5 {
    private int hours, minutes, seconds;

    public Task5(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public Task5() {
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if ( hours <= 23 ) {
            this.hours = hours;
        }
        else{
            this.hours = 0;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if ( minutes <= 59 ) {
            this.minutes = minutes;
        }
        else{
            this.minutes = 0;
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if ( seconds <= 59 ) {
            this.seconds = seconds;
        }
        else{
            this.seconds = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task5 task5 = (Task5) o;

        if (hours != task5.hours) return false;
        if (minutes != task5.minutes) return false;
        return seconds == task5.seconds;
    }

    @Override
    public int hashCode() {
        int result = hours;
        result = 31 * result + minutes;
        result = 31 * result + seconds;
        return result;
    }

    @Override
    public String toString() {
        return "Task5{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }


    public void addHours(int a){
        int newHours = hours + a;
        if( newHours <= 23 ){
            hours = newHours;
        }
    }

    public void addMinutes(int a){
        int newMinutes = minutes + a;
        if( newMinutes <= 59 ){
            minutes = newMinutes;
        }
    }

    public void addSeconds(int a){
        int newSeconds = seconds + a;
        if( newSeconds <= 59 ){
            seconds = newSeconds;
        }
    }
}
