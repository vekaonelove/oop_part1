public class Task4 {
    private int min, max, current;
    public Task4(int min, int max, int current) {
        this.min = min;
        this.max = max;
        this.current = current;

        if (max < min) {
            int temp = max;
            max = min;
            min = temp;
        }

        if (current < min)
            current = min;
        if (current > max)
            current = max;
    }

    public Task4(int min_, int max_) {
        this(min_, max_, min_);
        current = min;
    }

    public Task4() {
        this(0, 16, 0);
    }

    public void increaseByOne() {
        current++;
        if (current > max)
            current = min;
    }
    public void decreaseByOne() {
        current--;
        if (current < min)
            current = max;
    }
    public int value() {
        return current;
    }
}
